﻿using System;
using System.IO;
using System.Management;
using System.Security.Cryptography;
using System.Security;
using System.Collections;
using System.Text;


/// <summary>
/// Generates a 16 byte Unique Identification code of a computer
/// Example: 4876-8DB5-EE85-69D3-FE52-8CF7-395D-2EA9
/// </summary>
public class RegistrationModule
{
    private static string SharedKey = "1FBD5517";
    public static string MachineCode = "";
    public static string VerificationCode = "";
    public static bool IsRegistered = false;

    public static void GenerateID(string ProductId, string RegCode)
    {
        if (String.IsNullOrEmpty(MachineCode))
        {
            // generate Machine ID
            string mid = "Product ID >> " + ProductId + "\r\nBASE >> " + baseboardId() + "\r\nBIOS >> " + biosId() + "\r\nCPU >> " + cpuId();
            MachineCode = GetHashString(mid);
        }

        // generate varification ID
        VerificationCode = GetHashString(Encrypt(ProductId + MachineCode, SharedKey));

        // check registration
        IsRegistered = VerificationCode.Equals(RegCode);
    }

    public static string GenerateRegistrationCode(string ProductId, string MachineID, string Key)
    {
        string RegCode = Encrypt(ProductId + MachineID, Key);
        return GetHashString(RegCode);
    }

    public static bool CheckRegistrationCode(string ProductId, string RegCode)
    {
        IsRegistered = VerificationCode.Equals(RegCode);
        return IsRegistered;
    }

    private static string Encrypt(string PlainString, string SharedKey)
    {
        DESCryptoServiceProvider desProvider = new DESCryptoServiceProvider();
        desProvider.Mode = CipherMode.ECB;
        desProvider.Padding = PaddingMode.PKCS7;
        desProvider.Key = Encoding.ASCII.GetBytes(SharedKey);
        using (MemoryStream stream = new MemoryStream())
        {
            using (CryptoStream cs = new CryptoStream(stream, desProvider.CreateEncryptor(), CryptoStreamMode.Write))
            {
                byte[] data = Encoding.Default.GetBytes(PlainString);
                cs.Write(data, 0, data.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(stream.ToArray());
            }
        }
    }

    public static string GetHashString(string inputString)
    {
        int i = 0;
        HashAlgorithm ha = MD5.Create();  // SHA1.Create()
        StringBuilder sb = new StringBuilder();
        byte[] HashByte = ha.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        foreach (byte b in HashByte)
        {
            sb.Append(b.ToString("X2"));
            if (++i == 2) { i = 0; sb.Append(b.ToString("-")); }
        }
        sb.Remove(sb.Length - 1, 1);
        return sb.ToString();
    }

    #region Original Device ID Getting Code

    private static string cpuId()
    {
        //Uses first CPU identifier available in order of preference
        //Don't get all identifiers, as very time consuming
        string retVal = identifier("Win32_Processor", "UniqueId");
        if (retVal == "") //If no UniqueID, use ProcessorID
        {
            retVal = identifier("Win32_Processor", "ProcessorId");
            if (retVal == "") //If no ProcessorId, use Name
            {
                retVal = identifier("Win32_Processor", "Name");
                if (retVal == "") //If no Name, use Manufacturer
                {
                    retVal = identifier("Win32_Processor", "Manufacturer");
                }
                //Add clock speed for extra security
                retVal += identifier("Win32_Processor", "MaxClockSpeed");
            }
        }
        return retVal;
    }

    //BIOS Identifier
    private static string biosId()
    {
        return identifier("Win32_BIOS", "Manufacturer")
        + identifier("Win32_BIOS", "SMBIOSBIOSVersion")
        + identifier("Win32_BIOS", "IdentificationCode")
        + identifier("Win32_BIOS", "SerialNumber")
        + identifier("Win32_BIOS", "ReleaseDate")
        + identifier("Win32_BIOS", "Version");
    }

    //Main physical hard drive ID
    private static string diskId()
    {
        return identifier("Win32_DiskDrive", "Model")
        + identifier("Win32_DiskDrive", "Manufacturer")
        + identifier("Win32_DiskDrive", "Signature")
        + identifier("Win32_DiskDrive", "TotalHeads");
    }

    //Motherboard ID
    private static string baseboardId()
    {
        return identifier("Win32_BaseBoard", "Model")
        + identifier("Win32_BaseBoard", "Manufacturer")
        + identifier("Win32_BaseBoard", "Name")
        + identifier("Win32_BaseBoard", "SerialNumber");
    }

    //Primary video controller ID
    private static string videoId()
    {
        return identifier("Win32_VideoController", "DriverVersion")
        + identifier("Win32_VideoController", "Name");
    }

    //First enabled network card ID
    private static string macId()
    {
        return identifier("Win32_NetworkAdapterConfiguration", "MACAddress", "IPEnabled");
    }

    //Return a hardware identifier
    private static string identifier(string wmiClass, string wmiProperty, string wmiMustBeTrue)
    {
        string result = "";
        ManagementClass mc = new ManagementClass(wmiClass);
        ManagementObjectCollection moc = mc.GetInstances();
        foreach (ManagementObject mo in moc)
        {
            if (mo[wmiMustBeTrue].ToString() == "True")
            {
                //Only get the first one
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                    }
                }
            }
        }
        return result;
    }

    //Return a hardware identifier
    private static string identifier(string wmiClass, string wmiProperty)
    {
        string result = "";
        ManagementClass mc = new ManagementClass(wmiClass);
        ManagementObjectCollection moc = mc.GetInstances();
        foreach (ManagementObject mo in moc)
        {
            //Only get the first one
            if (result == "")
            {
                try
                {
                    result = mo[wmiProperty].ToString();
                    break;
                }
                catch
                {
                }
            }
        }
        return result;
    }
    #endregion
}