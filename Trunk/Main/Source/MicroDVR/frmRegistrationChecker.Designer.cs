﻿partial class frmRegistrationChecker
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegistrationChecker));
        this.btnExit = new System.Windows.Forms.Button();
        this.btnRegister = new System.Windows.Forms.Button();
        this.tbxProductId = new System.Windows.Forms.TextBox();
        this.tbxMachineId = new System.Windows.Forms.TextBox();
        this.pbrProgress = new System.Windows.Forms.ProgressBar();
        this.label1 = new System.Windows.Forms.Label();
        this.label2 = new System.Windows.Forms.Label();
        this.label3 = new System.Windows.Forms.Label();
        this.label4 = new System.Windows.Forms.Label();
        this.lblRegStatus = new System.Windows.Forms.Label();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.tbxRegCode = new System.Windows.Forms.TextBox();
        this.groupBox1.SuspendLayout();
        this.SuspendLayout();
        // 
        // btnExit
        // 
        this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
        this.btnExit.Location = new System.Drawing.Point(361, 257);
        this.btnExit.Name = "btnExit";
        this.btnExit.Size = new System.Drawing.Size(83, 34);
        this.btnExit.TabIndex = 0;
        this.btnExit.Text = "Exit";
        this.btnExit.UseVisualStyleBackColor = true;
        this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
        // 
        // btnRegister
        // 
        this.btnRegister.Cursor = System.Windows.Forms.Cursors.Hand;
        this.btnRegister.Location = new System.Drawing.Point(6, 257);
        this.btnRegister.Name = "btnRegister";
        this.btnRegister.Size = new System.Drawing.Size(83, 34);
        this.btnRegister.TabIndex = 1;
        this.btnRegister.Text = "Register";
        this.btnRegister.UseVisualStyleBackColor = true;
        this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
        // 
        // tbxProductId
        // 
        this.tbxProductId.Location = new System.Drawing.Point(6, 34);
        this.tbxProductId.Name = "tbxProductId";
        this.tbxProductId.ReadOnly = true;
        this.tbxProductId.Size = new System.Drawing.Size(116, 20);
        this.tbxProductId.TabIndex = 2;
        this.tbxProductId.Text = "MicroDVR";
        // 
        // tbxMachineId
        // 
        this.tbxMachineId.Location = new System.Drawing.Point(6, 74);
        this.tbxMachineId.Multiline = true;
        this.tbxMachineId.Name = "tbxMachineId";
        this.tbxMachineId.ReadOnly = true;
        this.tbxMachineId.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.tbxMachineId.Size = new System.Drawing.Size(438, 80);
        this.tbxMachineId.TabIndex = 3;
        // 
        // pbrProgress
        // 
        this.pbrProgress.Location = new System.Drawing.Point(12, 10);
        this.pbrProgress.Name = "pbrProgress";
        this.pbrProgress.Size = new System.Drawing.Size(450, 20);
        this.pbrProgress.TabIndex = 5;
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Location = new System.Drawing.Point(3, 18);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(61, 13);
        this.label1.TabIndex = 6;
        this.label1.Text = "Product ID:";
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Location = new System.Drawing.Point(6, 58);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(65, 13);
        this.label2.TabIndex = 7;
        this.label2.Text = "Machine ID:";
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Location = new System.Drawing.Point(6, 157);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(94, 13);
        this.label3.TabIndex = 8;
        this.label3.Text = "Registration Code:";
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Location = new System.Drawing.Point(155, 36);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(99, 13);
        this.label4.TabIndex = 9;
        this.label4.Text = "Registration Status:";
        // 
        // lblRegStatus
        // 
        this.lblRegStatus.AutoSize = true;
        this.lblRegStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblRegStatus.ForeColor = System.Drawing.Color.Red;
        this.lblRegStatus.Location = new System.Drawing.Point(260, 34);
        this.lblRegStatus.Name = "lblRegStatus";
        this.lblRegStatus.Size = new System.Drawing.Size(98, 16);
        this.lblRegStatus.TabIndex = 10;
        this.lblRegStatus.Text = "Unregistered";
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.label4);
        this.groupBox1.Controls.Add(this.lblRegStatus);
        this.groupBox1.Controls.Add(this.btnExit);
        this.groupBox1.Controls.Add(this.btnRegister);
        this.groupBox1.Controls.Add(this.label3);
        this.groupBox1.Controls.Add(this.tbxProductId);
        this.groupBox1.Controls.Add(this.label2);
        this.groupBox1.Controls.Add(this.tbxMachineId);
        this.groupBox1.Controls.Add(this.label1);
        this.groupBox1.Controls.Add(this.tbxRegCode);
        this.groupBox1.Location = new System.Drawing.Point(12, 12);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(450, 297);
        this.groupBox1.TabIndex = 11;
        this.groupBox1.TabStop = false;
        this.groupBox1.Visible = false;
        // 
        // tbxRegCode
        // 
        this.tbxRegCode.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::MicroDVR.Properties.Settings.Default, "__RegCode", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.tbxRegCode.Location = new System.Drawing.Point(6, 173);
        this.tbxRegCode.Multiline = true;
        this.tbxRegCode.Name = "tbxRegCode";
        this.tbxRegCode.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.tbxRegCode.Size = new System.Drawing.Size(438, 80);
        this.tbxRegCode.TabIndex = 4;
        this.tbxRegCode.Text = global::MicroDVR.Properties.Settings.Default.@__RegCode;
        // 
        // frmRegistrationChecker
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(474, 41);
        this.ControlBox = false;
        this.Controls.Add(this.groupBox1);
        this.Controls.Add(this.pbrProgress);
        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
        this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        this.MaximizeBox = false;
        this.Name = "frmRegistrationChecker";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "Please wait.....";
        this.groupBox1.ResumeLayout(false);
        this.groupBox1.PerformLayout();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnExit;
    private System.Windows.Forms.Button btnRegister;
    private System.Windows.Forms.TextBox tbxMachineId;
    private System.Windows.Forms.TextBox tbxRegCode;
    private System.Windows.Forms.ProgressBar pbrProgress;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label lblRegStatus;
    private System.Windows.Forms.GroupBox groupBox1;
    public System.Windows.Forms.TextBox tbxProductId;
}