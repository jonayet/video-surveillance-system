﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using iSpy.Video.FFMPEG;

namespace MicroDVR
{
    static class Program
    {
        public static Mutex WriterMutex;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // make settings portable
            PortableSettingsProvider PortableSettingsProvider = new PortableSettingsProvider();
            Properties.Settings.Default.Providers.Add(PortableSettingsProvider);
            foreach (System.Configuration.SettingsProperty property in Properties.Settings.Default.Properties)
            {
                property.Provider = PortableSettingsProvider;
            }
            // make settings portable
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            // chek registration
            Application.Run(new frmRegistrationChecker());
            if (!RegistrationModule.IsRegistered) { return; }
            // chek registration

            WriterMutex = new Mutex();
            var ffmpegSetup = new Init();
            ffmpegSetup.Initialise();
            Application.Run(new frmMain());
            WriterMutex.Close();
            ffmpegSetup.DeInitialise();
        }
    }
}
