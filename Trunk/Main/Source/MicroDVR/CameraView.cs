﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.ExceptionServices;
using AForge.Video;
using AForge.Video.DirectShow;
using iSpy.Video.FFMPEG;
using Encoder = System.Drawing.Imaging.Encoder;
using Image = System.Drawing.Image;
using PictureBox = AForge.Controls.PictureBox;

namespace MicroDVR
{
    public sealed class CameraView : PictureBox
    {
        private VideoCaptureDevice Camera;
        private Bitmap _lastFrame;
        private VideoFileWriter _writer;
        private readonly List<VideoFrame> _writerBuffer = new List<VideoFrame>();
        private readonly ManualResetEvent _stopWrite = new ManualResetEvent(false);
        private DateTime _lastRedraw = DateTime.MinValue;
        private Thread _recordingThread;
        private Size _videoSize = new Size(0, 0);

        private readonly object _lockobject = new object();
        private bool _IsOpen = false;
        private bool _IsMinimised = false;
        private static int _instanceId = 1;
        private string _instanceTitle = "";
        private string _recordingFolderPath = "";
        private string _monikerString = "";
        private int MaxRedrawRate = 15;

        public CameraView()
        {
            _instanceTitle = "CAMERA" + _instanceId.ToString();
            _instanceId++;
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.ResizeRedraw | ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint, true);
            this.BackColor = Color.Black;
        }

        private bool _opening = false;
        public delegate void CallingDelegate();
        public void Open()
        {
            if (_opening) { return; }

            if (InvokeRequired)
            {
                Invoke(new CallingDelegate(Open));
                return;
            }

            lock (_lockobject)
            {
                if (_IsOpen) { return; }
                _IsOpen = true;
            }

            _opening = true;
            try
            {
                ClearWriterBuffer();
                Camera = new VideoCaptureDevice(_monikerString);
                Camera.NewFrame -= new NewFrameEventHandler(Camera_NewFrame);
                Camera.NewFrame += new NewFrameEventHandler(Camera_NewFrame);
                Camera.Start();
            }
            catch { }
            _opening = false;
        }

        private bool _closing = false;
        public void Close()
        {
            if (_closing)
                return;

            if (InvokeRequired)
            {
                Invoke(new CallingDelegate(Close));
                return;
            }

            lock (_lockobject)
            {
                if (!_IsOpen) { return; }
                _IsOpen = false;
            }

            _closing = true;
            try
            {
                Application.DoEvents();
                FinishRecording();
                if (Camera != null)
                {
                    Camera.NewFrame -= Camera_NewFrame;

                    if (Camera != null)
                    {
                        Application.DoEvents();
                        lock (_lockobject)
                        {
                            try
                            {
                                Camera.SignalToStop();
                                Camera.WaitForStop();
                            }
                            catch { }
                        }
                    }

                    Camera = null;
                }

                ClearWriterBuffer();
                LastFrame = null;

                GC.Collect();
            }
            catch { }
            _closing = false;
        }

        public void StartRecording()
        {
            if (Recording) { return; }

            if (InvokeRequired)
            {
                Invoke(new CallingDelegate(StartRecording));
                return;
            }

            lock (_lockobject)
            {
                _recordingThread = new Thread(DoRecord)
                {
                    Name = _instanceId.ToString(),
                    IsBackground = false,
                    Priority = ThreadPriority.Normal
                };
                _recordingThread.Start();
            }
        }

        [HandleProcessCorruptedStateExceptions]
        private void DoRecord()
        {
            _stopWrite.Reset();
            ClearWriterBuffer();
            DateTime recordingStart = DateTime.UtcNow;

            try
            {
                DateTime date = DateTime.Now;
                string filename = String.Format("{0}-{1:00}-{2:00}_{3}-{4:00}-{5:00}", date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second) + ".mp4";
                string videoPath = _recordingFolderPath + "\\" + _instanceTitle + "_" + filename;

                try
                {
                    if (!Directory.Exists(_recordingFolderPath)) { Directory.CreateDirectory(_recordingFolderPath); }

                    try
                    {
                        Program.WriterMutex.WaitOne();
                        _writer = new VideoFileWriter();
                        _writer.Open(videoPath, _videoSize.Width, _videoSize.Height, VideoCodec.H264, 80000, 0);
                    }
                    finally { Program.WriterMutex.ReleaseMutex(); }

                    bool first = true;
                    while (!_stopWrite.WaitOne(5))
                    {
                        while (_writerBuffer.Count > 0 && !_stopWrite.WaitOne(0))
                        {
                            var fa = _writerBuffer[0];
                            if (fa.Frame != null)
                            {
                                if (first)
                                {
                                    recordingStart = fa.TimeStamp;
                                    first = false;
                                }

                                using (var ms = new MemoryStream(fa.Frame))
                                {
                                    using (var bmp = (Bitmap)Image.FromStream(ms))
                                    {
                                        var pts = (long)(fa.TimeStamp - recordingStart).TotalMilliseconds;
                                        if (pts >= 0)
                                        {
                                            try
                                            {
                                                _writer.WriteVideoFrame(bmp, pts);
                                            }
                                            catch { }
                                        }
                                    }
                                    ms.Close();
                                }
                            }
                            fa.Nullify();
                            _writerBuffer.RemoveAt(0);
                        }
                    }
                }
                catch { }

                if (_writer != null && _writer.IsOpen)
                {
                    try
                    {
                        Program.WriterMutex.WaitOne();
                        _writer.Close();
                        _writer.Dispose();
                    }
                    finally { Program.WriterMutex.ReleaseMutex(); }

                    _writer = null;
                }
            }
            catch { }
        }

        public void FinishRecording()
        {
            if (!Recording) { return; }

            if (InvokeRequired)
            {
                Invoke(new CallingDelegate(FinishRecording));
                return;
            }

            if (Recording)
            {
                _stopWrite.Set();
                _recordingThread.Join();
            }
        }

        private void Camera_NewFrame(object sender, NewFrameEventArgs e)
        {
            if (_videoSize.Width == 0 || _videoSize.Width == 0)
            {
                _videoSize = new Size(e.Frame.Width, e.Frame.Height);
            }

            using (Bitmap resizedframe = ResizeBitmap(e.Frame))
            using (Graphics gFrame = Graphics.FromImage(resizedframe))
            using (var textFont = new Font(FontFamily.GenericSansSerif, 16, FontStyle.Regular, GraphicsUnit.Pixel))
            using (var textBrush = new SolidBrush(Color.White))
            using (var backBrush = new SolidBrush(Color.FromArgb(100, Color.Black)))
            {
                try
                {
                    string timeStamp = _instanceTitle + " " + String.Format("{0: - d/MM/yyyy - hh:mm:ss tt}", DateTime.Now);
                    var s = gFrame.MeasureString(timeStamp, textFont);
                    gFrame.FillRectangle(backBrush, 0, 0, s.Width, s.Height);
                    gFrame.DrawString(timeStamp, textFont, textBrush, new PointF(4, 0));

                    lock (_lockobject)
                    {
                        if (Recording)
                        {
                            _writerBuffer.Add(new VideoFrame(resizedframe, DateTime.UtcNow));
                        }
                    }

                    if (_lastRedraw < DateTime.UtcNow.AddMilliseconds(0 - 1000 / MaxRedrawRate))
                    {
                        LastFrame = resizedframe;
                    }

                    if (NewFrame != null) { NewFrame(this, e); }
                }
                catch { }
            }

        }

        private int _blinkCounter = 0;
        protected override void OnPaint(PaintEventArgs pe)
        {
            Graphics gCam = pe.Graphics;
            using (var volBrush = new SolidBrush(Color.Red))
            using (var textFont = new Font(FontFamily.GenericSansSerif, 14, FontStyle.Bold, GraphicsUnit.Pixel))
            {
                Rectangle rc = ClientRectangle;
                try
                {
                    if (_IsOpen)
                    {
                        if (GotImage)
                        {
                            gCam.Clear(Color.Black);
                            gCam.CompositingMode = CompositingMode.SourceCopy;
                            gCam.CompositingQuality = CompositingQuality.HighSpeed;
                            gCam.PixelOffsetMode = PixelOffsetMode.HighSpeed;
                            gCam.SmoothingMode = SmoothingMode.None;
                            gCam.InterpolationMode = InterpolationMode.Default;
                            gCam.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;

                            if (!_IsMinimised)
                            {
                                var bmp = LastFrame;

                                if (bmp != null)
                                {
                                    gCam.DrawImage(bmp, 2, 2, rc.Width - 4, rc.Height - 4);
                                    bmp.Dispose();
                                }
                            }
                            gCam.CompositingMode = CompositingMode.SourceOver;
                        }

                        if (Recording)
                        {
                            if (_blinkCounter < 4)
                            {
                                gCam.FillEllipse(volBrush, new Rectangle(rc.Width - 54, 4, 12, 12));
                                gCam.DrawString("REC", textFont, volBrush, new Point(rc.Width - 40, 2));
                            }
                            if (++_blinkCounter >= 8) { _blinkCounter = 0; }
                        }
                    }
                    else
                    {
                        gCam.DrawString(_instanceTitle + ": Offline", textFont, volBrush, new PointF(5, 5));
                    }
                }
                catch { }
            }

            base.OnPaint(pe);
            _lastRedraw = DateTime.UtcNow;
        }

        protected override void OnResize(EventArgs eventargs)
        {
            base.OnResize(eventargs);
            _IsMinimised = Size.Equals(MinimumSize);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Invalidate();
            }
            _writer = null;
            base.Dispose(disposing);
        }

        private void ClearWriterBuffer()
        {
            lock (_lockobject)
            {
                while (_writerBuffer.Count > 0)
                {
                    _writerBuffer[0].Nullify();
                    _writerBuffer.RemoveAt(0);
                }
            }
        }

        private Bitmap ResizeBitmap(Bitmap frame)
        {
            if (frame.Width == _videoSize.Width && frame.Height == _videoSize.Height) { return frame; }

            var b = new Bitmap(_videoSize.Width, _videoSize.Height);
            var r = new Rectangle(0, 0, _videoSize.Width, _videoSize.Height);
            using (var g = Graphics.FromImage(b))
            {
                g.CompositingMode = CompositingMode.SourceCopy;
                g.CompositingQuality = CompositingQuality.HighSpeed;
                g.PixelOffsetMode = PixelOffsetMode.HighSpeed;
                g.SmoothingMode = SmoothingMode.None;
                g.InterpolationMode = InterpolationMode.Default;

                //g.GdiDrawImage(LastFrame, r);
                g.DrawImage(frame, r);
            }

            frame.Dispose();
            frame = null;
            return b;
        }

        public event NewFrameEventHandler NewFrame;

        internal bool GotImage;
        public Bitmap LastFrame
        {
            get
            {
                lock (_lockobject)
                {
                    if (_lastFrame == null)
                    {
                        return null;
                    }
                    return (Bitmap)_lastFrame.Clone();
                }
            }

            set
            {
                if (_IsOpen)
                {
                    lock (_lockobject)
                    {
                        if (_lastFrame != null)
                            _lastFrame.Dispose();

                        if (value != null)
                        {
                            _lastFrame = (Bitmap)value.Clone();
                        }
                        else
                        {
                            _lastFrame = null;
                        }
                    }
                }

                GotImage = value != null;
                Invalidate();
            }
        }

        public string Title { set { _instanceTitle = value; Invalidate(); } get { return _instanceTitle; } }

        public string MonikerString { set { _monikerString = value; } get { return _monikerString; } }

        public string RecordingFolderPath { set { _recordingFolderPath = value; } get { return _recordingFolderPath; } }

        public Size VideoSize { set { _videoSize = value; Invalidate(); } get { return _videoSize; } }

        public bool Recording
        {
            get
            {
                try
                {
                    return _recordingThread != null && !_recordingThread.Join(TimeSpan.Zero);
                }
                catch { }
                return false;
            }
        }

        public bool IsOpen { get { return _IsOpen; } }

        #region Nested type: VideoFrame
        private struct VideoFrame
        {
            public byte[] Frame;
            public readonly DateTime TimeStamp;

            public VideoFrame(Bitmap frame, DateTime timeStamp)
            {
                TimeStamp = timeStamp;

                ImageCodecInfo _encoder = ImageCodecInfo.GetImageEncoders()[0];
                ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
                foreach (ImageCodecInfo codec in codecs)
                {
                    if (codec.FormatID == ImageFormat.Jpeg.Guid)
                    {
                        _encoder = codec;
                    }
                }

                int JPEGQuality = 100;
                EncoderParameters _encoderParams = new EncoderParameters(1);
                _encoderParams.Param[0] = new EncoderParameter(Encoder.Quality, JPEGQuality);

                using (var ms = new MemoryStream())
                {
                    frame.Save(ms, _encoder, _encoderParams);
                    Frame = ms.GetBuffer();
                }
            }

            public void Nullify()
            {
                Frame = null;
            }
        }
        #endregion
    }
}