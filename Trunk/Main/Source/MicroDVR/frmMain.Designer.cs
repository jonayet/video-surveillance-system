﻿namespace MicroDVR
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cameraView1 = new MicroDVR.CameraView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cameraView2 = new MicroDVR.CameraView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cameraView3 = new MicroDVR.CameraView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cameraView4 = new MicroDVR.CameraView();
            this.OpenRecordingDirectory = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.buttonFocusCam4 = new System.Windows.Forms.Button();
            this.buttonResetFocus = new System.Windows.Forms.Button();
            this.buttonFocusCam3 = new System.Windows.Forms.Button();
            this.buttonFocusCam2 = new System.Windows.Forms.Button();
            this.buttonFocusCam1 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblRecordTime = new System.Windows.Forms.Label();
            this.btnRecord = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tmrStartRec = new System.Windows.Forms.Timer(this.components);
            this.btnSettings = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraView2)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraView3)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cameraView1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(313, 250);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Camera 1";
            // 
            // cameraView1
            // 
            this.cameraView1.BackColor = System.Drawing.Color.Black;
            this.cameraView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraView1.Image = null;
            this.cameraView1.LastFrame = null;
            this.cameraView1.Location = new System.Drawing.Point(3, 16);
            this.cameraView1.MinimumSize = new System.Drawing.Size(50, 50);
            this.cameraView1.MonikerString = "";
            this.cameraView1.Name = "cameraView1";
            this.cameraView1.RecordingFolderPath = "";
            this.cameraView1.Size = new System.Drawing.Size(307, 231);
            this.cameraView1.TabIndex = 0;
            this.cameraView1.TabStop = false;
            this.cameraView1.Title = "CAMERA1";
            this.cameraView1.VideoSize = new System.Drawing.Size(0, 0);
            this.cameraView1.DoubleClick += new System.EventHandler(this.cameraView1_DoubleClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cameraView2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(313, 250);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Camera 2";
            // 
            // cameraView2
            // 
            this.cameraView2.BackColor = System.Drawing.Color.Black;
            this.cameraView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraView2.Image = null;
            this.cameraView2.LastFrame = null;
            this.cameraView2.Location = new System.Drawing.Point(3, 16);
            this.cameraView2.MinimumSize = new System.Drawing.Size(50, 50);
            this.cameraView2.MonikerString = "";
            this.cameraView2.Name = "cameraView2";
            this.cameraView2.RecordingFolderPath = "";
            this.cameraView2.Size = new System.Drawing.Size(307, 231);
            this.cameraView2.TabIndex = 0;
            this.cameraView2.TabStop = false;
            this.cameraView2.Title = "CAMERA2";
            this.cameraView2.VideoSize = new System.Drawing.Size(0, 0);
            this.cameraView2.DoubleClick += new System.EventHandler(this.cameraView2_DoubleClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cameraView3);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(313, 245);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Camera 3";
            // 
            // cameraView3
            // 
            this.cameraView3.BackColor = System.Drawing.Color.Black;
            this.cameraView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraView3.Image = null;
            this.cameraView3.LastFrame = null;
            this.cameraView3.Location = new System.Drawing.Point(3, 16);
            this.cameraView3.MinimumSize = new System.Drawing.Size(50, 50);
            this.cameraView3.MonikerString = "";
            this.cameraView3.Name = "cameraView3";
            this.cameraView3.RecordingFolderPath = "";
            this.cameraView3.Size = new System.Drawing.Size(307, 226);
            this.cameraView3.TabIndex = 0;
            this.cameraView3.TabStop = false;
            this.cameraView3.Title = "CAMERA3";
            this.cameraView3.VideoSize = new System.Drawing.Size(0, 0);
            this.cameraView3.DoubleClick += new System.EventHandler(this.cameraView3_DoubleClick);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cameraView4);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(313, 245);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Camera 4";
            // 
            // cameraView4
            // 
            this.cameraView4.BackColor = System.Drawing.Color.Black;
            this.cameraView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cameraView4.Image = null;
            this.cameraView4.LastFrame = null;
            this.cameraView4.Location = new System.Drawing.Point(3, 16);
            this.cameraView4.MinimumSize = new System.Drawing.Size(50, 50);
            this.cameraView4.MonikerString = "";
            this.cameraView4.Name = "cameraView4";
            this.cameraView4.RecordingFolderPath = "";
            this.cameraView4.Size = new System.Drawing.Size(307, 226);
            this.cameraView4.TabIndex = 0;
            this.cameraView4.TabStop = false;
            this.cameraView4.Title = "CAMERA4";
            this.cameraView4.VideoSize = new System.Drawing.Size(0, 0);
            this.cameraView4.DoubleClick += new System.EventHandler(this.cameraView4_DoubleClick);
            // 
            // OpenRecordingDirectory
            // 
            this.OpenRecordingDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenRecordingDirectory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OpenRecordingDirectory.Location = new System.Drawing.Point(653, 398);
            this.OpenRecordingDirectory.Name = "OpenRecordingDirectory";
            this.OpenRecordingDirectory.Size = new System.Drawing.Size(112, 50);
            this.OpenRecordingDirectory.TabIndex = 2;
            this.OpenRecordingDirectory.Text = "Open Folder";
            this.OpenRecordingDirectory.UseVisualStyleBackColor = true;
            this.OpenRecordingDirectory.Click += new System.EventHandler(this.OpenRecordingDirectory_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(12, 31);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel1MinSize = 0;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Panel2MinSize = 0;
            this.splitContainer1.Size = new System.Drawing.Size(630, 499);
            this.splitContainer1.SplitterDistance = 250;
            this.splitContainer1.TabIndex = 3;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer2.Panel1MinSize = 0;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer2.Panel2MinSize = 0;
            this.splitContainer2.Size = new System.Drawing.Size(630, 250);
            this.splitContainer2.SplitterDistance = 313;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.groupBox3);
            this.splitContainer3.Panel1MinSize = 0;
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.groupBox4);
            this.splitContainer3.Panel2MinSize = 0;
            this.splitContainer3.Size = new System.Drawing.Size(630, 245);
            this.splitContainer3.SplitterDistance = 313;
            this.splitContainer3.TabIndex = 0;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.buttonFocusCam4);
            this.groupBox10.Controls.Add(this.buttonResetFocus);
            this.groupBox10.Controls.Add(this.buttonFocusCam3);
            this.groupBox10.Controls.Add(this.buttonFocusCam2);
            this.groupBox10.Controls.Add(this.buttonFocusCam1);
            this.groupBox10.Location = new System.Drawing.Point(648, 31);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(124, 188);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Focus";
            // 
            // buttonFocusCam4
            // 
            this.buttonFocusCam4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonFocusCam4.Location = new System.Drawing.Point(12, 119);
            this.buttonFocusCam4.Name = "buttonFocusCam4";
            this.buttonFocusCam4.Size = new System.Drawing.Size(99, 23);
            this.buttonFocusCam4.TabIndex = 0;
            this.buttonFocusCam4.Text = "Camera 4";
            this.buttonFocusCam4.UseVisualStyleBackColor = true;
            this.buttonFocusCam4.Click += new System.EventHandler(this.buttonFocusCam4_Click);
            // 
            // buttonResetFocus
            // 
            this.buttonResetFocus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonResetFocus.Location = new System.Drawing.Point(12, 152);
            this.buttonResetFocus.Name = "buttonResetFocus";
            this.buttonResetFocus.Size = new System.Drawing.Size(99, 23);
            this.buttonResetFocus.TabIndex = 0;
            this.buttonResetFocus.Text = "Reset View";
            this.buttonResetFocus.UseVisualStyleBackColor = true;
            this.buttonResetFocus.Click += new System.EventHandler(this.buttonResetFocus_Click);
            // 
            // buttonFocusCam3
            // 
            this.buttonFocusCam3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonFocusCam3.Location = new System.Drawing.Point(12, 86);
            this.buttonFocusCam3.Name = "buttonFocusCam3";
            this.buttonFocusCam3.Size = new System.Drawing.Size(99, 23);
            this.buttonFocusCam3.TabIndex = 0;
            this.buttonFocusCam3.Text = "Camera 3";
            this.buttonFocusCam3.UseVisualStyleBackColor = true;
            this.buttonFocusCam3.Click += new System.EventHandler(this.buttonFocusCam3_Click);
            // 
            // buttonFocusCam2
            // 
            this.buttonFocusCam2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonFocusCam2.Location = new System.Drawing.Point(12, 53);
            this.buttonFocusCam2.Name = "buttonFocusCam2";
            this.buttonFocusCam2.Size = new System.Drawing.Size(99, 23);
            this.buttonFocusCam2.TabIndex = 0;
            this.buttonFocusCam2.Text = "Camera 2";
            this.buttonFocusCam2.UseVisualStyleBackColor = true;
            this.buttonFocusCam2.Click += new System.EventHandler(this.buttonFocusCam2_Click);
            // 
            // buttonFocusCam1
            // 
            this.buttonFocusCam1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonFocusCam1.Location = new System.Drawing.Point(12, 20);
            this.buttonFocusCam1.Name = "buttonFocusCam1";
            this.buttonFocusCam1.Size = new System.Drawing.Size(99, 23);
            this.buttonFocusCam1.TabIndex = 0;
            this.buttonFocusCam1.Text = "Camera 1";
            this.buttonFocusCam1.UseVisualStyleBackColor = true;
            this.buttonFocusCam1.Click += new System.EventHandler(this.buttonFocusCam1_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.lblTime);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.lblRecordTime);
            this.groupBox5.Controls.Add(this.btnRecord);
            this.groupBox5.Location = new System.Drawing.Point(648, 225);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(124, 167);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Recording";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Time:";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.Location = new System.Drawing.Point(7, 139);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(96, 25);
            this.lblTime.TabIndex = 3;
            this.lblTime.Text = "00:00:00";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Length:";
            // 
            // lblRecordTime
            // 
            this.lblRecordTime.AutoSize = true;
            this.lblRecordTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecordTime.Location = new System.Drawing.Point(6, 94);
            this.lblRecordTime.Name = "lblRecordTime";
            this.lblRecordTime.Size = new System.Drawing.Size(96, 25);
            this.lblRecordTime.TabIndex = 1;
            this.lblRecordTime.Text = "00:00:00";
            // 
            // btnRecord
            // 
            this.btnRecord.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRecord.Enabled = false;
            this.btnRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecord.ForeColor = System.Drawing.Color.ForestGreen;
            this.btnRecord.Location = new System.Drawing.Point(5, 23);
            this.btnRecord.Name = "btnRecord";
            this.btnRecord.Size = new System.Drawing.Size(112, 44);
            this.btnRecord.TabIndex = 0;
            this.btnRecord.Text = "RECORD";
            this.btnRecord.UseVisualStyleBackColor = true;
            this.btnRecord.Click += new System.EventHandler(this.btnRecord_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tmrStartRec
            // 
            this.tmrStartRec.Interval = 1000;
            this.tmrStartRec.Tick += new System.EventHandler(this.tmrStartRec_Tick);
            // 
            // btnSettings
            // 
            this.btnSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSettings.Location = new System.Drawing.Point(653, 454);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(112, 28);
            this.btnSettings.TabIndex = 5;
            this.btnSettings.Text = "Settings";
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(649, 501);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(124, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.OpenRecordingDirectory);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MicroDVR 2.0";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cameraView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cameraView2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cameraView3)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cameraView4)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button OpenRecordingDirectory;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button buttonFocusCam4;
        private System.Windows.Forms.Button buttonResetFocus;
        private System.Windows.Forms.Button buttonFocusCam3;
        private System.Windows.Forms.Button buttonFocusCam2;
        private System.Windows.Forms.Button buttonFocusCam1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnRecord;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRecordTime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer tmrStartRec;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.PictureBox pictureBox1;
        private CameraView cameraView1;
        private CameraView cameraView2;
        private CameraView cameraView3;
        private CameraView cameraView4;
    }
}

