﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MicroDVR.Properties;

// Libraries needed to work with VideoInputDevices
using AForge.Video;
using AForge.Video.DirectShow;



namespace MicroDVR
{
    public partial class frmMain : Form
    {
        // Refrence to cameraMonitors of all 4 cams
        CameraView[] CamMonitor = new CameraView[4];
        
        // Indexed arrays containing referces to the user interface components
        // so they can be easily accessed later on
        GroupBox[] camPanels = new GroupBox[4];
        Button[] camButton = new Button[4];
        bool[] recordCamera = new bool[4];
        
        System.Timers.Timer MinTimer = new System.Timers.Timer();
        DateTime RecStartTime = DateTime.Now;
        bool IsFullscreen = false;
        bool IsRecording = false;


        public frmMain()
        {
            InitializeComponent();

            camPanels[0] = groupBox1;
            camPanels[1] = groupBox2;
            camPanels[2] = groupBox3;
            camPanels[3] = groupBox4;

            camButton[0] = buttonFocusCam1;
            camButton[1] = buttonFocusCam2;
            camButton[2] = buttonFocusCam3;
            camButton[3] = buttonFocusCam4;

            System.Diagnostics.Process myProcess = System.Diagnostics.Process.GetCurrentProcess();
            myProcess.PriorityClass = System.Diagnostics.ProcessPriorityClass.High;
        }

        // When the form loads
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // initializ all the cameras
            InitCameras();

            // start recording after 1 second
            if (Settings.Default.__AutoStartRecording && btnRecord.Enabled)
            {
                tmrStartRec.Enabled = true;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            CloseCameras();
            base.OnClosing(e);
        }

        private void InitCameras()
        {
            // we disable all the user controls (will be activated later when we load cameras)
            // displays the current frame on the main form
            camPanels[0].Enabled = false;
            camPanels[1].Enabled = false;
            camPanels[2].Enabled = false;
            camPanels[3].Enabled = false;
            camButton[0].Enabled = false;
            camButton[1].Enabled = false;
            camButton[2].Enabled = false;
            camButton[3].Enabled = false;
            recordCamera[0] = Settings.Default.__Cam1Record;
            recordCamera[1] = Settings.Default.__Cam2Record;
            recordCamera[2] = Settings.Default.__Cam3Record;
            recordCamera[3] = Settings.Default.__Cam4Record;
            btnRecord.Enabled = false;

            // an instance of FilterInfoCollection is created to fetch available VideoCaptureDevices
            FilterInfoCollection webcam = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            // Camera 1
            int SrcIndex = Settings.Default.__Cam1Src;
            if (SrcIndex >= 0 && SrcIndex < webcam.Count)
            {
                if (Settings.Default.__Cam1Enabled)
                {
                    CamMonitor[0] = cameraView1;
                    CamMonitor[0].MonikerString = webcam[SrcIndex].MonikerString;
                    CamMonitor[0].VideoSize = new Size(320, 240);
                    CamMonitor[0].Title = "CAM1";
                    CamMonitor[0].Open();
                    camPanels[0].Enabled = true;
                    camButton[0].Enabled = true;
                    if (recordCamera[0]) { btnRecord.Enabled = true; }
                }
            }

            // Camera 2
            SrcIndex = Settings.Default.__Cam2Src;
            if (SrcIndex >= 0 && SrcIndex < webcam.Count)
            {
                if (Settings.Default.__Cam2Enabled)
                {
                    CamMonitor[1] = cameraView2;
                    CamMonitor[1].MonikerString = webcam[SrcIndex].MonikerString;
                    CamMonitor[1].VideoSize = new Size(320, 240);
                    CamMonitor[1].Title = "CAM2";
                    CamMonitor[1].Open();
                    camPanels[1].Enabled = true;
                    camButton[1].Enabled = true;
                    if (recordCamera[1]) { btnRecord.Enabled = true; }
                }
            }

            // Camera 3
            SrcIndex = Settings.Default.__Cam3Src;
            if (SrcIndex >= 0 && SrcIndex < webcam.Count)
            {
                if (Settings.Default.__Cam3Enabled)
                {
                    CamMonitor[2] = cameraView3;
                    CamMonitor[2].MonikerString = webcam[SrcIndex].MonikerString;
                    CamMonitor[2].VideoSize = new Size(320, 240);
                    CamMonitor[2].Title = "CAM3";
                    CamMonitor[2].Open();
                    camPanels[2].Enabled = true;
                    camButton[2].Enabled = true;
                    if (recordCamera[2]) { btnRecord.Enabled = true; }
                }
            }

            // Camera 4
            SrcIndex = Settings.Default.__Cam4Src;
            if (SrcIndex >= 0 && SrcIndex < webcam.Count)
            {
                if (Settings.Default.__Cam4Enabled)
                {
                    CamMonitor[3] = cameraView4;
                    CamMonitor[3].MonikerString = webcam[SrcIndex].MonikerString;
                    CamMonitor[3].VideoSize = new Size(320, 240);
                    CamMonitor[3].Title = "CAM4";
                    CamMonitor[3].Open();
                    camPanels[3].Enabled = true;
                    camButton[3].Enabled = true;
                    if (recordCamera[3]) { btnRecord.Enabled = true; }
                }
            }

            if (Settings.Default.__RecordingPath == "")
            {
                // if the recording path is not set previously by the user
                // this code below will prompt the user to set it
                FolderBrowserDialog folder = new FolderBrowserDialog();
                while (folder.SelectedPath == "")
                {
                    folder.ShowDialog();
                }

                if (folder.SelectedPath != "")
                {
                    Settings.Default["__RecordingPath"] = folder.SelectedPath;
                    Settings.Default.Save();
                }
            }

            // set the recording path to the exising CameraMonitors
            for (int i = 0; i < 4; i++)
            {
                try
                {
                    CamMonitor[i].RecordingFolderPath = Settings.Default.__RecordingPath;
                }
                catch { }
            }
        }

        // this method will start recording of running cameras 
        private void StartRecording()
        {
            if (IsRecording) { return; }
            for (int i = 0; i < 4; i++)
            {
                try
                {
                    if (recordCamera[i]) { CamMonitor[i].StartRecording(); }
                    IsRecording = true;
                }
                catch { }
            }
            MinuteCounter = 0;
        }
        
        // this method will stop recording of running cameras 
        private void StopRecording()
        {
            if (!IsRecording) { return; }
            for (int i = 0; i < 4; i++)
            {
                try
                {
                    if (recordCamera[i]) { CamMonitor[i].FinishRecording(); }
                    IsRecording = false;
                }
                catch { }
            }
        }

        // this method will stop recording and running cameras 
        private void CloseCameras()
        {
            StopRecording();
            for (int i = 0; i < 4; i++)
            {
                try
                {
                    CamMonitor[i].Close();
                }
                catch { }
            }
        }

        // Opening the record directory 
        private void OpenRecordingDirectory_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", Settings.Default.__RecordingPath);
        }

        private void SetFocus(int camIndex)
        {
            switch (camIndex)
            {
                case 0:
                    splitContainer1.Panel1Collapsed = false;
                    splitContainer1.Panel2Collapsed = false;
                    splitContainer2.Panel1Collapsed = false;
                    splitContainer2.Panel2Collapsed = false;
                    splitContainer3.Panel1Collapsed = false;
                    splitContainer3.Panel2Collapsed = false;
                    IsFullscreen = false;
                    break;
                case 1:
                    splitContainer1.Panel2Collapsed = true;
                    splitContainer2.Panel2Collapsed = true;
                    IsFullscreen = true;
                    break;
                case 2:
                    splitContainer1.Panel2Collapsed = true;
                    splitContainer2.Panel1Collapsed = true;
                    IsFullscreen = true;
                    break;
                case 3:
                    splitContainer1.Panel1Collapsed = true;
                    splitContainer3.Panel2Collapsed = true;
                    IsFullscreen = true;
                    break;
                case 4:
                    splitContainer1.Panel1Collapsed = true;
                    splitContainer3.Panel1Collapsed = true;
                    IsFullscreen = true;
                    break;
            }
        }

        private void buttonFocusCam1_Click(object sender, EventArgs e)
        {
            SetFocus(1);
        }

        private void buttonFocusCam2_Click(object sender, EventArgs e)
        {
            SetFocus(2);
        }

        private void buttonFocusCam3_Click(object sender, EventArgs e)
        {
            SetFocus(3);
        }

        private void buttonFocusCam4_Click(object sender, EventArgs e)
        {
            SetFocus(4);
        }

        private void buttonResetFocus_Click(object sender, EventArgs e)
        {
            SetFocus(0);
        }

        private void cameraView1_DoubleClick(object sender, EventArgs e)
        {
            if (!IsFullscreen)
            {
                SetFocus(1);
            }
            else
            {
                SetFocus(0);
            }
        }

        private void cameraView2_DoubleClick(object sender, EventArgs e)
        {
            if (!IsFullscreen)
            {
                SetFocus(2);
                IsFullscreen = true;
            }
            else
            {
                SetFocus(0);
                IsFullscreen = false;
            }
        }

        private void cameraView3_DoubleClick(object sender, EventArgs e)
        {
            if (!IsFullscreen)
            {
                SetFocus(3);
                IsFullscreen = true;
            }
            else
            {
                SetFocus(0);
                IsFullscreen = false;
            }
        }

        private void cameraView4_DoubleClick(object sender, EventArgs e)
        {
            if (!IsFullscreen)
            {
                SetFocus(4);
                IsFullscreen = true;
            }
            else
            {
                SetFocus(0);
                IsFullscreen = false;
            }
        }

        private void btnRecord_Click(object sender, EventArgs e)
        {
            if (IsRecording)
            {
                MinTimer.Stop();
                StopRecording();
                btnRecord.Text = "RECORD";
                btnRecord.ForeColor = Color.ForestGreen;
            }
            else
            {
                StartRecording();
                MinTimer.Start();
                RecStartTime = DateTime.Now;
                btnRecord.Text = "STOP";
                btnRecord.ForeColor = Color.Red;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = String.Format("{0:hh:mm:ss}", DateTime.Now);
            if (IsRecording)
            {
                TimeSpan span = DateTime.Now - RecStartTime;
                lblRecordTime.Text = String.Format("{0:00}:{1:00}:{2:00}", span.Hours, span.Minutes, span.Seconds);
            }
        }

        private void tmrStartRec_Tick(object sender, EventArgs e)
        {
            StartRecording();
            RecStartTime = DateTime.Now;
            btnRecord.Text = "STOP";
            btnRecord.ForeColor = Color.Red;

            // start 1 minute timer
            MinTimer.AutoReset = true;
            MinTimer.Elapsed += new System.Timers.ElapsedEventHandler(MinTimer_Tick);
            MinTimer.Interval = 60 * 1000;
            MinTimer.Start();

            // stop this timer
            tmrStartRec.Enabled = false;
        }

        int MinuteCounter = 0;
        void MinTimer_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            MinuteCounter++;
            if (Settings.Default.__SplitIndex == 0)
            {
                MinuteCounter = 0;
            }
            else if (Settings.Default.__SplitIndex == 1)
            {
                if (MinuteCounter >= 30)
                {
                    StopRecording();
                    StartRecording();
                    MinuteCounter = 0;
                }
            }
            else if (Settings.Default.__SplitIndex == 2)
            {
                if (MinuteCounter >= 60)
                {
                    StopRecording();
                    StartRecording();
                    MinuteCounter = 0;
                }
            }
            else if (Settings.Default.__SplitIndex == 3)
            {
                if (MinuteCounter >= 180)
                {
                    StopRecording();
                    StartRecording();
                    MinuteCounter = 0;
                }
            }
        }

        frmSettings frmSettings;
        bool IsRecordingBeforeSettings = false;
        private void btnSettings_Click(object sender, EventArgs e)
        {
            IsRecordingBeforeSettings = IsRecording;
            frmSettings = new frmSettings();
            frmSettings.FormClosed += new FormClosedEventHandler(frmSettings_FormClosed);
            frmSettings.ShowDialog();
        }

        void frmSettings_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (frmSettings.IsSettingChanged)
            {
                // re initializa all the cameras
                CloseCameras();
                btnRecord.Text = "RECORD";
                btnRecord.ForeColor = Color.ForestGreen;

                InitCameras();
                MinTimer.Stop();
                if (IsRecordingBeforeSettings && btnRecord.Enabled) { tmrStartRec.Enabled = true; }
            }
        }
    }
}
