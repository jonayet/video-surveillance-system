﻿namespace MicroDVR
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSettings));
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cbxCam4Record = new System.Windows.Forms.CheckBox();
            this.cbxCam4Enabled = new System.Windows.Forms.CheckBox();
            this.cbxCam3Record = new System.Windows.Forms.CheckBox();
            this.cbxCam3Enabled = new System.Windows.Forms.CheckBox();
            this.cbxCam2Record = new System.Windows.Forms.CheckBox();
            this.cbxCam2Enabled = new System.Windows.Forms.CheckBox();
            this.cbxCam1Record = new System.Windows.Forms.CheckBox();
            this.cbxCam1Enabled = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbxCam4Src = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbxCam3Src = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbxCam2Src = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbxCam1Src = new System.Windows.Forms.ComboBox();
            this.btnSaveExit = new System.Windows.Forms.Button();
            this.cmbxSplitTime = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbxAutoStartRecordint = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ObtnOpenRecDir = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.RecordingPathInput = new System.Windows.Forms.TextBox();
            this.btnChangeRecPath = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cbxCam4Record);
            this.groupBox6.Controls.Add(this.cbxCam4Enabled);
            this.groupBox6.Controls.Add(this.cbxCam3Record);
            this.groupBox6.Controls.Add(this.cbxCam3Enabled);
            this.groupBox6.Controls.Add(this.cbxCam2Record);
            this.groupBox6.Controls.Add(this.cbxCam2Enabled);
            this.groupBox6.Controls.Add(this.cbxCam1Record);
            this.groupBox6.Controls.Add(this.cbxCam1Enabled);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.cmbxCam4Src);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.cmbxCam3Src);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.cmbxCam2Src);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.cmbxCam1Src);
            this.groupBox6.Location = new System.Drawing.Point(13, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(381, 135);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Camera Source";
            // 
            // cbxCam4Record
            // 
            this.cbxCam4Record.AutoSize = true;
            this.cbxCam4Record.Checked = global::MicroDVR.Properties.Settings.Default.@__Cam4Record;
            this.cbxCam4Record.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxCam4Record.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::MicroDVR.Properties.Settings.Default, "__Cam4Record", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxCam4Record.Enabled = false;
            this.cbxCam4Record.Location = new System.Drawing.Point(317, 103);
            this.cbxCam4Record.Name = "cbxCam4Record";
            this.cbxCam4Record.Size = new System.Drawing.Size(61, 17);
            this.cbxCam4Record.TabIndex = 19;
            this.cbxCam4Record.Text = "Record";
            this.cbxCam4Record.UseVisualStyleBackColor = true;
            this.cbxCam4Record.CheckedChanged += new System.EventHandler(this.cbxCam4Record_CheckedChanged);
            // 
            // cbxCam4Enabled
            // 
            this.cbxCam4Enabled.AutoSize = true;
            this.cbxCam4Enabled.Checked = global::MicroDVR.Properties.Settings.Default.@__Cam4Enabled;
            this.cbxCam4Enabled.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::MicroDVR.Properties.Settings.Default, "__Cam4Enabled", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxCam4Enabled.Location = new System.Drawing.Point(254, 103);
            this.cbxCam4Enabled.Name = "cbxCam4Enabled";
            this.cbxCam4Enabled.Size = new System.Drawing.Size(59, 17);
            this.cbxCam4Enabled.TabIndex = 18;
            this.cbxCam4Enabled.Text = "Enable";
            this.cbxCam4Enabled.UseVisualStyleBackColor = true;
            this.cbxCam4Enabled.CheckedChanged += new System.EventHandler(this.cbxCam4Enabled_CheckedChanged);
            // 
            // cbxCam3Record
            // 
            this.cbxCam3Record.AutoSize = true;
            this.cbxCam3Record.Checked = global::MicroDVR.Properties.Settings.Default.@__Cam3Record;
            this.cbxCam3Record.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxCam3Record.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::MicroDVR.Properties.Settings.Default, "__Cam3Record", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxCam3Record.Enabled = false;
            this.cbxCam3Record.Location = new System.Drawing.Point(317, 76);
            this.cbxCam3Record.Name = "cbxCam3Record";
            this.cbxCam3Record.Size = new System.Drawing.Size(61, 17);
            this.cbxCam3Record.TabIndex = 17;
            this.cbxCam3Record.Text = "Record";
            this.cbxCam3Record.UseVisualStyleBackColor = true;
            this.cbxCam3Record.CheckedChanged += new System.EventHandler(this.cbxCam3Record_CheckedChanged);
            // 
            // cbxCam3Enabled
            // 
            this.cbxCam3Enabled.AutoSize = true;
            this.cbxCam3Enabled.Checked = global::MicroDVR.Properties.Settings.Default.@__Cam3Enabled;
            this.cbxCam3Enabled.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::MicroDVR.Properties.Settings.Default, "__Cam3Enabled", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxCam3Enabled.Location = new System.Drawing.Point(254, 76);
            this.cbxCam3Enabled.Name = "cbxCam3Enabled";
            this.cbxCam3Enabled.Size = new System.Drawing.Size(59, 17);
            this.cbxCam3Enabled.TabIndex = 16;
            this.cbxCam3Enabled.Text = "Enable";
            this.cbxCam3Enabled.UseVisualStyleBackColor = true;
            this.cbxCam3Enabled.CheckedChanged += new System.EventHandler(this.cbxCam3Enabled_CheckedChanged);
            // 
            // cbxCam2Record
            // 
            this.cbxCam2Record.AutoSize = true;
            this.cbxCam2Record.Checked = global::MicroDVR.Properties.Settings.Default.@__Cam2Record;
            this.cbxCam2Record.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxCam2Record.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::MicroDVR.Properties.Settings.Default, "__Cam2Record", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxCam2Record.Enabled = false;
            this.cbxCam2Record.Location = new System.Drawing.Point(317, 49);
            this.cbxCam2Record.Name = "cbxCam2Record";
            this.cbxCam2Record.Size = new System.Drawing.Size(61, 17);
            this.cbxCam2Record.TabIndex = 15;
            this.cbxCam2Record.Text = "Record";
            this.cbxCam2Record.UseVisualStyleBackColor = true;
            this.cbxCam2Record.CheckedChanged += new System.EventHandler(this.cbxCam2Record_CheckedChanged);
            // 
            // cbxCam2Enabled
            // 
            this.cbxCam2Enabled.AutoSize = true;
            this.cbxCam2Enabled.Checked = global::MicroDVR.Properties.Settings.Default.@__Cam2Enabled;
            this.cbxCam2Enabled.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::MicroDVR.Properties.Settings.Default, "__Cam2Enabled", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxCam2Enabled.Location = new System.Drawing.Point(254, 49);
            this.cbxCam2Enabled.Name = "cbxCam2Enabled";
            this.cbxCam2Enabled.Size = new System.Drawing.Size(59, 17);
            this.cbxCam2Enabled.TabIndex = 14;
            this.cbxCam2Enabled.Text = "Enable";
            this.cbxCam2Enabled.UseVisualStyleBackColor = true;
            this.cbxCam2Enabled.CheckedChanged += new System.EventHandler(this.cbxCam2Enabled_CheckedChanged);
            // 
            // cbxCam1Record
            // 
            this.cbxCam1Record.AutoSize = true;
            this.cbxCam1Record.Checked = global::MicroDVR.Properties.Settings.Default.@__Cam1Record;
            this.cbxCam1Record.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxCam1Record.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::MicroDVR.Properties.Settings.Default, "__Cam1Record", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxCam1Record.Enabled = false;
            this.cbxCam1Record.Location = new System.Drawing.Point(317, 23);
            this.cbxCam1Record.Name = "cbxCam1Record";
            this.cbxCam1Record.Size = new System.Drawing.Size(61, 17);
            this.cbxCam1Record.TabIndex = 13;
            this.cbxCam1Record.Text = "Record";
            this.cbxCam1Record.UseVisualStyleBackColor = true;
            this.cbxCam1Record.CheckedChanged += new System.EventHandler(this.cbxCam1Record_CheckedChanged);
            // 
            // cbxCam1Enabled
            // 
            this.cbxCam1Enabled.AutoSize = true;
            this.cbxCam1Enabled.Checked = global::MicroDVR.Properties.Settings.Default.@__Cam1Enabled;
            this.cbxCam1Enabled.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::MicroDVR.Properties.Settings.Default, "__Cam1Enabled", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxCam1Enabled.Location = new System.Drawing.Point(254, 23);
            this.cbxCam1Enabled.Name = "cbxCam1Enabled";
            this.cbxCam1Enabled.Size = new System.Drawing.Size(59, 17);
            this.cbxCam1Enabled.TabIndex = 12;
            this.cbxCam1Enabled.Text = "Enable";
            this.cbxCam1Enabled.UseVisualStyleBackColor = true;
            this.cbxCam1Enabled.CheckedChanged += new System.EventHandler(this.cbxCam1Enabled_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Camera 4:";
            // 
            // cmbxCam4Src
            // 
            this.cmbxCam4Src.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxCam4Src.FormattingEnabled = true;
            this.cmbxCam4Src.Location = new System.Drawing.Point(76, 101);
            this.cmbxCam4Src.Name = "cmbxCam4Src";
            this.cmbxCam4Src.Size = new System.Drawing.Size(169, 21);
            this.cmbxCam4Src.TabIndex = 10;
            this.cmbxCam4Src.SelectedIndexChanged += new System.EventHandler(this.cmbxCam4Src_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Camera 3:";
            // 
            // cmbxCam3Src
            // 
            this.cmbxCam3Src.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxCam3Src.FormattingEnabled = true;
            this.cmbxCam3Src.Location = new System.Drawing.Point(76, 74);
            this.cmbxCam3Src.Name = "cmbxCam3Src";
            this.cmbxCam3Src.Size = new System.Drawing.Size(169, 21);
            this.cmbxCam3Src.TabIndex = 8;
            this.cmbxCam3Src.SelectedIndexChanged += new System.EventHandler(this.cmbxCam3Src_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Camera 2:";
            // 
            // cmbxCam2Src
            // 
            this.cmbxCam2Src.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxCam2Src.FormattingEnabled = true;
            this.cmbxCam2Src.Location = new System.Drawing.Point(76, 47);
            this.cmbxCam2Src.Name = "cmbxCam2Src";
            this.cmbxCam2Src.Size = new System.Drawing.Size(169, 21);
            this.cmbxCam2Src.TabIndex = 6;
            this.cmbxCam2Src.SelectedIndexChanged += new System.EventHandler(this.cmbxCam2Src_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Camera 1:";
            // 
            // cmbxCam1Src
            // 
            this.cmbxCam1Src.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxCam1Src.FormattingEnabled = true;
            this.cmbxCam1Src.Location = new System.Drawing.Point(76, 20);
            this.cmbxCam1Src.Name = "cmbxCam1Src";
            this.cmbxCam1Src.Size = new System.Drawing.Size(169, 21);
            this.cmbxCam1Src.TabIndex = 5;
            this.cmbxCam1Src.SelectedIndexChanged += new System.EventHandler(this.cmbxCam1Src_SelectedIndexChanged);
            // 
            // btnSaveExit
            // 
            this.btnSaveExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveExit.Location = new System.Drawing.Point(298, 325);
            this.btnSaveExit.Name = "btnSaveExit";
            this.btnSaveExit.Size = new System.Drawing.Size(96, 32);
            this.btnSaveExit.TabIndex = 8;
            this.btnSaveExit.Text = "Save && Exit";
            this.btnSaveExit.UseVisualStyleBackColor = true;
            this.btnSaveExit.Click += new System.EventHandler(this.btnSaveExit_Click);
            // 
            // cmbxSplitTime
            // 
            this.cmbxSplitTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxSplitTime.FormattingEnabled = true;
            this.cmbxSplitTime.Items.AddRange(new object[] {
            "No Split",
            "30 Min",
            "1 Hour",
            "3 Hour"});
            this.cmbxSplitTime.Location = new System.Drawing.Point(71, 17);
            this.cmbxSplitTime.Name = "cmbxSplitTime";
            this.cmbxSplitTime.Size = new System.Drawing.Size(118, 21);
            this.cmbxSplitTime.TabIndex = 9;
            this.cmbxSplitTime.SelectedIndexChanged += new System.EventHandler(this.cmbxSplitTime_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbxAutoStartRecordint);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.ObtnOpenRecDir);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.RecordingPathInput);
            this.groupBox1.Controls.Add(this.cmbxSplitTime);
            this.groupBox1.Controls.Add(this.btnChangeRecPath);
            this.groupBox1.Location = new System.Drawing.Point(12, 153);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(382, 157);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Recording";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "info@synergyinterface.com";
            // 
            // cbxAutoStartRecordint
            // 
            this.cbxAutoStartRecordint.AutoSize = true;
            this.cbxAutoStartRecordint.Checked = true;
            this.cbxAutoStartRecordint.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxAutoStartRecordint.Location = new System.Drawing.Point(195, 19);
            this.cbxAutoStartRecordint.Name = "cbxAutoStartRecordint";
            this.cbxAutoStartRecordint.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbxAutoStartRecordint.Size = new System.Drawing.Size(176, 17);
            this.cbxAutoStartRecordint.TabIndex = 16;
            this.cbxAutoStartRecordint.Text = "Start Recording at program start";
            this.cbxAutoStartRecordint.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "www.synergyinterface.com";
            // 
            // ObtnOpenRecDir
            // 
            this.ObtnOpenRecDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ObtnOpenRecDir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ObtnOpenRecDir.Location = new System.Drawing.Point(286, 122);
            this.ObtnOpenRecDir.Name = "ObtnOpenRecDir";
            this.ObtnOpenRecDir.Size = new System.Drawing.Size(90, 27);
            this.ObtnOpenRecDir.TabIndex = 15;
            this.ObtnOpenRecDir.Text = "Open";
            this.ObtnOpenRecDir.UseVisualStyleBackColor = true;
            this.ObtnOpenRecDir.Click += new System.EventHandler(this.ObtnOpenRecDir_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Split After:";
            // 
            // RecordingPathInput
            // 
            this.RecordingPathInput.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::MicroDVR.Properties.Settings.Default, "__RecordingPath", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.RecordingPathInput.Location = new System.Drawing.Point(7, 47);
            this.RecordingPathInput.Multiline = true;
            this.RecordingPathInput.Name = "RecordingPathInput";
            this.RecordingPathInput.ReadOnly = true;
            this.RecordingPathInput.Size = new System.Drawing.Size(369, 70);
            this.RecordingPathInput.TabIndex = 12;
            this.RecordingPathInput.Text = global::MicroDVR.Properties.Settings.Default.@__RecordingPath;
            // 
            // btnChangeRecPath
            // 
            this.btnChangeRecPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangeRecPath.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChangeRecPath.Location = new System.Drawing.Point(188, 122);
            this.btnChangeRecPath.Name = "btnChangeRecPath";
            this.btnChangeRecPath.Size = new System.Drawing.Size(92, 27);
            this.btnChangeRecPath.TabIndex = 13;
            this.btnChangeRecPath.Text = "Change";
            this.btnChangeRecPath.UseVisualStyleBackColor = true;
            this.btnChangeRecPath.Click += new System.EventHandler(this.btnChangeRecPath_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 317);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 40);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // frmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 370);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSaveExit);
            this.Controls.Add(this.groupBox6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbxCam4Src;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbxCam3Src;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbxCam2Src;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbxCam1Src;
        private System.Windows.Forms.Button btnSaveExit;
        private System.Windows.Forms.ComboBox cmbxSplitTime;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnChangeRecPath;
        private System.Windows.Forms.TextBox RecordingPathInput;
        private System.Windows.Forms.Button ObtnOpenRecDir;
        private System.Windows.Forms.CheckBox cbxAutoStartRecordint;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox cbxCam1Record;
        private System.Windows.Forms.CheckBox cbxCam1Enabled;
        private System.Windows.Forms.CheckBox cbxCam4Record;
        private System.Windows.Forms.CheckBox cbxCam4Enabled;
        private System.Windows.Forms.CheckBox cbxCam3Record;
        private System.Windows.Forms.CheckBox cbxCam3Enabled;
        private System.Windows.Forms.CheckBox cbxCam2Record;
        private System.Windows.Forms.CheckBox cbxCam2Enabled;
    }
}