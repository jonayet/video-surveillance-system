﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

// Libraries needed to work with VideoInputDevices
using AForge.Video;
using AForge.Video.DirectShow;


namespace MicroDVR
{
    public partial class frmSettings : Form
    {
        public bool IsSettingChanged = false;

        public frmSettings()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // an instance of FilterInfoCollection is created to fetch available VideoCaptureDevices
            FilterInfoCollection webcam = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            for(int i = 0; i < webcam.Count; i++)
            {
                cmbxCam1Src.Items.Add((i + 1).ToString() + ". " + webcam[i].Name);
                cmbxCam2Src.Items.Add((i + 1).ToString() + ". " + webcam[i].Name);
                cmbxCam3Src.Items.Add((i + 1).ToString() + ". " + webcam[i].Name);
                cmbxCam4Src.Items.Add((i + 1).ToString() + ". " + webcam[i].Name);
            }

            if (Properties.Settings.Default.__Cam1Src < webcam.Count)
            {
                cmbxCam1Src.SelectedIndex = Properties.Settings.Default.__Cam1Src;
            }

            if (Properties.Settings.Default.__Cam2Src < webcam.Count)
            {
                cmbxCam2Src.SelectedIndex = Properties.Settings.Default.__Cam2Src;
            }

            if (Properties.Settings.Default.__Cam3Src < webcam.Count)
            {
                cmbxCam3Src.SelectedIndex = Properties.Settings.Default.__Cam3Src;
            }

            if (Properties.Settings.Default.__Cam4Src < webcam.Count + 1)
            {
                cmbxCam4Src.SelectedIndex = Properties.Settings.Default.__Cam4Src;
            }

            cbxCam1Record.Enabled = cbxCam1Enabled.Checked;
            cbxCam2Record.Enabled = cbxCam2Enabled.Checked;
            cbxCam3Record.Enabled = cbxCam3Enabled.Checked;
            cbxCam4Record.Enabled = cbxCam4Enabled.Checked;

            cbxAutoStartRecordint.Checked = Properties.Settings.Default.__AutoStartRecording;
            cmbxSplitTime.SelectedIndex = Properties.Settings.Default.__SplitIndex;
            IsSettingChanged = false;
        }

        private void btnSaveExit_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default["__Cam1Src"] = cmbxCam1Src.SelectedIndex;
            Properties.Settings.Default["__Cam2Src"] = cmbxCam2Src.SelectedIndex;
            Properties.Settings.Default["__Cam3Src"] = cmbxCam3Src.SelectedIndex;
            Properties.Settings.Default["__Cam4Src"] = cmbxCam4Src.SelectedIndex;
            Properties.Settings.Default["__SplitIndex"] = cmbxSplitTime.SelectedIndex;
            Properties.Settings.Default["__AutoStartRecording"] = cbxAutoStartRecordint.Checked;
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void btnChangeRecPath_Click(object sender, EventArgs e)
        {
            // prompt the user with a FolderBrowserDialog
            FolderBrowserDialog folder = new FolderBrowserDialog();
            folder.ShowDialog();

            if (folder.SelectedPath != "")
            {
                RecordingPathInput.Text = folder.SelectedPath;
                IsSettingChanged = true;
            }
        }

        private void ObtnOpenRecDir_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", RecordingPathInput.Text);
        }

        private void cmbxCam1Src_SelectedIndexChanged(object sender, EventArgs e)
        {
            IsSettingChanged = true;
        }

        private void cmbxCam2Src_SelectedIndexChanged(object sender, EventArgs e)
        {
            IsSettingChanged = true;
        }

        private void cmbxCam3Src_SelectedIndexChanged(object sender, EventArgs e)
        {
            IsSettingChanged = true;
        }

        private void cmbxCam4Src_SelectedIndexChanged(object sender, EventArgs e)
        {
            IsSettingChanged = true;
        }

        private void cmbxSplitTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            IsSettingChanged = true;
        }

        private void cbxCam1Enabled_CheckedChanged(object sender, EventArgs e)
        {
            cbxCam1Record.Enabled = cbxCam1Enabled.Checked;
            IsSettingChanged = true;
        }

        private void cbxCam2Enabled_CheckedChanged(object sender, EventArgs e)
        {
            cbxCam2Record.Enabled = cbxCam2Enabled.Checked;
            IsSettingChanged = true;
        }

        private void cbxCam3Enabled_CheckedChanged(object sender, EventArgs e)
        {
            cbxCam3Record.Enabled = cbxCam3Enabled.Checked;
            IsSettingChanged = true;
        }

        private void cbxCam4Enabled_CheckedChanged(object sender, EventArgs e)
        {
            cbxCam4Record.Enabled = cbxCam4Enabled.Checked;
            IsSettingChanged = true;
        }

        private void cbxCam1Record_CheckedChanged(object sender, EventArgs e)
        {
            IsSettingChanged = true;
        }

        private void cbxCam2Record_CheckedChanged(object sender, EventArgs e)
        {
            IsSettingChanged = true;
        }

        private void cbxCam3Record_CheckedChanged(object sender, EventArgs e)
        {
            IsSettingChanged = true;
        }

        private void cbxCam4Record_CheckedChanged(object sender, EventArgs e)
        {
            IsSettingChanged = true;
        }
    }
}